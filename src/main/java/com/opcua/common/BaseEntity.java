package com.opcua.common;

import lombok.Data;

import java.util.Date;

/**
 * @author ykz
 */
@Data
public class BaseEntity {
    private Date createTime;
    private Date updateTime;
}

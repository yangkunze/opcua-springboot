package com.opcua.common;

import lombok.Data;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.nodes.UaNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;

@Data
public class MyUaNode extends UaNode {

    public MyUaNode(OpcUaClient client, NodeId nodeId, NodeClass nodeClass, QualifiedName browseName, LocalizedText displayName, LocalizedText description, UInteger writeMask, UInteger userWriteMask) {
        super(client, nodeId, nodeClass, browseName, displayName, description, writeMask, userWriteMask);
    }

    private Long id;

    private Integer equipmentType;

    private String equipmentName;

    public MyUaNode(Long id,Integer equipmentType,String equipmentName){
        super(null,new NodeId(2,equipmentName),null,null,null,null,null,null);
        this.id = id;
        this.equipmentType = equipmentType;
        this.equipmentName = equipmentName;
    }

    public MyUaNode(){
        super(null,null,null,null,null,null,null,null);
    }
}

package com.opcua.common;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;

import java.util.List;

public class Tree<T> {

   final List<Tree<T>> children = Lists.newCopyOnWriteArrayList();

   final T node;

   public Tree(T node) {
       this.node = node;
   }


   public void addChild(T child) {
       children.add(new Tree<>(child));
   }
   public void addChilds(Tree<T> child) {
       children.add(child);
   }

   @Override
   public String toString() {
       return MoreObjects.toStringHelper(this)
           .add("node", node)
           .add("children", children)
           .toString();
   }

    public T getNode() {
        return node;
    }

    public List<Tree<T>> getChildren() {
        return children;
    }
}
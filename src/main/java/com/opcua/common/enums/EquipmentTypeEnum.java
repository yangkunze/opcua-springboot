package com.opcua.common.enums;

public enum EquipmentTypeEnum {

    ONE(1,"通道"),
    TWO(2,"设备"),
    THREE(3,"标识组"),
    FOUR(4,"标识");

    private Integer type;
    private String desc;

    EquipmentTypeEnum(Integer type,String desc){
        this.desc = desc;
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}

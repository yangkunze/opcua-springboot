package com.opcua.server.service.impl;

import com.opcua.server.model.TbOpcPoint;
import com.opcua.server.mapper.TbOpcPointMapper;
import com.opcua.server.service.ITbOpcPointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kzyang
 * @since 2023-02-06
 */
@Service
public class TbOpcPointServiceImpl extends ServiceImpl<TbOpcPointMapper, TbOpcPoint> implements ITbOpcPointService {

}

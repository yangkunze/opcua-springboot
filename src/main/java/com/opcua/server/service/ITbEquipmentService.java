package com.opcua.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.opcua.common.MyUaNode;
import com.opcua.server.model.TbEquipment;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kzyang
 * @since 2023-02-06
 */
public interface ITbEquipmentService extends IService<TbEquipment> {
    Boolean asyncEquipment(MyUaNode uaNode) throws Exception;
}

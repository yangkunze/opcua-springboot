package com.opcua.server.service;

import com.opcua.server.model.TbOpcPoint;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kzyang
 * @since 2023-02-06
 */
public interface ITbOpcPointService extends IService<TbOpcPoint> {

}

package com.opcua.server.model;

import lombok.Data;

/**
 * @author ykz
 */
@Data
public class OpcUaParameter {
    private String fieldName;
    private String fieldNameIndex;
}

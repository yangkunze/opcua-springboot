package com.opcua.server.model;

import com.opcua.common.BaseEntity;
import lombok.Data;

@Data
public class TbEquipment extends BaseEntity {
    private Long id;

    /**
     * 设备名称
     */
    private String equipmentName;

    /**
     * 父级ID
     */
    private Long equipmentParent;

    /**
     * 设备类型
     */
    private Integer equipmentType;

    /**
     * 描述
     */
    private String description;

    /**
     * db地址
     */
    private String dbInfo;
}

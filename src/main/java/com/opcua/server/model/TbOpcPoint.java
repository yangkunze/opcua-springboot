package com.opcua.server.model;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author kzyang
 * @since 2023-02-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="TbOpcPoint对象", description="")
public class TbOpcPoint extends Model {

    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty(value = "订阅点信息（通道.设备.标识）")
    private String point;

    @ApiModelProperty(value = "订阅点名称")
    @TableField("point_name")
    private String pointName;


}

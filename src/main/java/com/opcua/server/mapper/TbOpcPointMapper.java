package com.opcua.server.mapper;

import com.opcua.server.model.TbOpcPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kzyang
 * @since 2023-02-06
 */
@Mapper
public interface TbOpcPointMapper extends BaseMapper<TbOpcPoint> {

}

package com.opcua.server.controller;


import com.opcua.common.MyUaNode;
import com.opcua.server.service.ITbEquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kzyang
 * @since 2023-02-06
 */
@RestController
@RequestMapping("/equipmentInfo")
public class TbOpcPointController {

    @Autowired
    ITbEquipmentService equipmentService;

    @PostMapping(value = "/syncEquipment")
    public Boolean syncEquipmentInfo(@RequestBody MyUaNode uaNode) throws Exception {
        return equipmentService.asyncEquipment(uaNode);
    }
}

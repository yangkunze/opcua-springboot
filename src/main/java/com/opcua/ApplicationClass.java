package com.opcua;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ykz
 */
@SpringBootApplication
@MapperScan(basePackages = "com.opcua.server.mapper")
public class ApplicationClass {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationClass.class,args);
    }
}

# opcua-springboot

#### 介绍
本人参与项目涉及到使用kepServer软件和下位PLC通信，软件对上用OPCUA协议，
由于java通过opcua协议连接kepServer资源少之又少，本人也是摸爬滚打半年左
右并结合项目需求做出套件形式的demo。  （目前没有前端支持，需要自行开发，后续会提供出所有接口文档）  
完全开源，希望有能力或是合理想法者能进行补充或是BUG修复以及优化
#### 实现功能
1.订阅KEPSERVER点位信息（完成）  
2.读取KEPSERVER点值（完成）  
3.对具体点位进行写值（完成）  
4.同步KEPSERVER点位信息并保存值数据库（完成）  
5.通过前端页面导入功能实现向KEPSERVER增加变量（孵化中）

#### 软件架构
SpringBoot框架、milo依赖包、redis缓存数据、mysql数据库

#### 安装教程

1.  java8开发环境
2.  kepServer6.x
3.  milo0.6.3

#### 使用说明

1.  更改opcua地址
2.  配置kepServer opcua信息
3.  新建通道、设备、点名

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
